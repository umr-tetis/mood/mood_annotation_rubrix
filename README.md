# Rubrix: Annotation tool in the context of mood
**[MOOD Rubrix](http://mo-mood-tetis-rubrix.montpellier.irstea.priv:6900)**

![rubrix](./readme.ressources/rubrix.png)
## Installation of rubrix pipeline
You need 2 compenents :
	+ A web server
	+ A list of python scripts which they are going to log information into the web server
### Rubrix web server
1. Follow instruction from [rubrix docs](https://rubrix.readthedocs.io/en/stable/getting_started/setup%26installation.html)
2. Start rubrix in a screen environnement :
```
screen -S rubrix
python3 -m rubrix
```
3. Connect to [rubrix](http://mo-mood-tetis-rubrix.montpellier.irstea.priv:6900/)

### Scripts that will use the rubrix API
1. Install your favourite NLP libraries. For example:
```
pip3 install torch
pip3 install datasets
pip3 install spacy
pip3 install spacy[transformers]
pip3 install transformers[torch]
```
2. Install your favourite NLP models:
```
python3 -m spacy download en_core_web_sm
python3 -m spacy download en_core_web_trf
```
3. write your script and use the rubrix API to log your information. Some key concepts: 
	+ Evaluate the label of NER extracted: see a example with [analyze_spacy_ner_trf.py](./analyze_spacy_ner_trf.py)
		+ Loop on document
		+ For each doc, get NER information (text, label_, positioning)
		+ Create a list with a rubrix object **TokenClassificationRecord** which contains:
			+ text
			+ list of token from the text
			+ the label of NER (with their positioning)
			+ The model used for the predictions
