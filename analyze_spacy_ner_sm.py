from datasets import load_dataset
import pandas as pd
import spacy
import rubrix as rb
from tqdm.auto import tqdm

dataset = load_dataset("gutenberg_time", split="train", streaming=True)
nlp = spacy.load("en_core_web_sm")

records = []

for record in tqdm(list(dataset.take(50))):
    text = record["tok_context"]

    doc = nlp(text)

    entities = [(
        ent.label_, ent.start_char, ent.end_char)
        for ent in doc.ents
    ]

    tokens = [token.text for token in doc]

    records.append(
            rb.TokenClassificationRecord(
                text=text,
                tokens=tokens,
                prediction=entities,
                prediction_agent="en_core_web_sm"
            )
    )
rb.log(records=records, name="gutenberg_spacy_ner")
